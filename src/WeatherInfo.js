import { View, Text, SafeAreaView, StyleSheet, Image, Dimensions, ScrollView } from 'react-native'
import React from 'react'
import { Icon } from '@rneui/themed';
import WeatherSearch from './search';

const WeatherInfo = ({ weatherData, fetchWeatherData}) => {
    const { name,
            visibility,
            weather: [{icon, description}],
            main: {temp, humidity, feels_like},
            wind: {speed},
            sys: {sunrise, sunset},
        } = weatherData

    return (
        <SafeAreaView style={styles.container}>
            <WeatherSearch fetchWeatherData={fetchWeatherData} />
            <View style={{alignItems: 'center'}}>
                <Text style={styles.title}>{name}</Text>
            </View>
            <View style={styles.logo}>
                <Image
                    style={styles.largeIcon}
                    source={{uri: `http://openweathermap.org/img/wn/${icon}.png`}}
                />
                <Text style={styles.currentTemp}>{temp} °C</Text>
            </View>
            <Text style={styles.description}>{description}</Text>

            <ScrollView>
                <View style={styles.extraInfo}>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='temperature-high'
                            type='font-awesome-5'
                            size='40'
                            color='#f50' />
                        <Text style={styles.infoText}>{feels_like} °C</Text>
                        <Text style={styles.infoText}>Feels Like</Text>
                    </View>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='droplet'
                            type='feather'
                            size='40'
                            color='#05C3DD' />
                        <Text style={styles.infoText}>{humidity} °C</Text>
                        <Text style={styles.infoText}>Humidity</Text>

                    </View>
                </View>

                <View style={styles.extraInfo}>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='eye'
                            type='font-awesome-5'
                            size='40'
                            color='#000' />
                        <Text style={styles.infoText}>{visibility}</Text>
                        <Text style={styles.infoText}>Visibility</Text>

                    </View>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='wind'
                            type='feather'
                            size='40'
                            color='#05C3DD' />
                        <Text style={styles.infoText}>{speed} m/s</Text>
                        <Text style={styles.infoText}>Wind Speed</Text>

                    </View>
                </View>

                <View style={styles.extraInfo}>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='sunrise'
                            type='feather'
                            size='40'
                            color='#FDB813' />
                        <Text style={styles.infoText}>{new Date(sunrise * 1000).toLocaleString()}</Text>
                        <Text style={styles.infoText}>Sunrise</Text>

                    </View>
                    <View style={styles.info}>
                        <Icon
                            style={styles.smallIcon}
                            name='sunset'
                            type='feather'
                            size='40'
                            color='#FDB813' />
                        <Text style={styles.infoText}>{new Date(sunset * 1000).toLocaleString()}</Text>
                        <Text style={styles.infoText}>Sunset</Text>

                    </View>
                </View>
            </ScrollView>

        </SafeAreaView>
    )
}

export default WeatherInfo

const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop: 15,
    },
    title: {
        width: '100%',
        textAlign: 'center',
        fontSize: 26,
        fontWeight: 'bold',
        color: "red",
        marginTop: 10,

    },
    logo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },

    largeIcon: {
        width: 100,
        height: 100,
    },
    currentTemp: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    description: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        color: "black",
        marginBottom: 10,
    },
    extraInfo: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 7,
    },

    info: {
        width: Dimensions.get('screen').width/2.5,
        backgroundColor: '#D0EAFA',
        padding:10,
        borderRadius: 5,
        justifyContent: 'center',

    },
    smallIcon:{
        borderRadius: 40/2,
    },
    infoText:{
        textAlign: 'center',
        fontSize: 18
    }

})