import { View, Text, StyleSheet, Alert, ActivityIndicator} from 'react-native'
import React, {useState, useEffect} from 'react'
import Constants from 'expo-constants'
import WeatherInfo from './WeatherInfo'

const API_KEYS = '548fc04181a0b58560d18532f1d00fdb'

const Weather = () => {
    const [weatherData, setWeatherData] = useState(null);
    const [loaded, setLoaded] = useState(false);

    const fetchWeatherData = async (cityName) => {
        try {
            setLoaded(false);
            const url = `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEYS}&units=metric`
            const response = await fetch(url)


            if(response.status == 200) {
                const data = await response.json()
                setWeatherData(data)

            }else{
                setWeatherData(null)
            }

            setLoaded(true)
        } catch (error) {
            Alert.alert('Error', error.message);
        }

    }

    useEffect(() => {
        fetchWeatherData('Manila')
    }, [])

    if(!loaded){
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" color="blue" />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Weather App</Text>
            </View>
            <WeatherInfo weatherData={weatherData} fetchWeatherData={fetchWeatherData} />
        </View>
    )
}

export default Weather

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: Constants.statusBarHeight,
    },
    header: {
        backgroundColor: '#FFF',
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontSize: 29,
        fontWeight: 'bold',
        color: '#000',
    },
});
